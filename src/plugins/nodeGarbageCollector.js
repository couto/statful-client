// @flow

import gc from "gc-stats";
import type Statful from "../Statful";

// @see https://github.com/nodejs/node/blob/554fa24916c5c6d052b51c5cee9556b76489b3f7/deps/v8/include/v8.h#L6137-L6144

// @HACK https://github.com/facebook/flow/issues/380#issuecomment-322204637
// I swear that sometimes I really don't understand some flowtype decisions...
const gcEnum = {
  /* ::[ */ 1 /* ::] */: "scavenge",
  /* ::[ */ 2 /* ::] */: "mark_sweep_compact",
  /* ::[ */ 4 /* ::] */: "incremental_marking",
  /* ::[ */ 8 /* ::] */: "weak_phantom_callbacks",
  /* ::[ */ 15 /* ::] */: "all"
};

type GCType = $Keys<typeof gcEnum>;

type GCHeapStats = {
  totalHeapSize: number,
  totalHeapExecutableSize: number,
  usedHeapSize: number,
  heapSizeLimit: number
};

type GCStats = {
  pause: number,
  pauseMS: number,
  gctype: GCType,
  before: GCHeapStats,
  after: GCHeapStats,
  diff: GCHeapStats
};

export default () => (statful: Statful): Statful => {
  gc().on("stats", (stats: GCStats) => {
    const type: GCType = gcEnum[stats.gctype];
    const memoryDiff = stats.diff.usedHeapSize;

    statful.timer("garbage_collection", stats.pause, { tags: { type } });
    
    if (memoryDiff < 0) {
      statful.counter("garbage_collection_reclaimed_bytes", memoryDiff * -1, {
        tags: { type }
      });
    }
  });
  
  

  return statful;
};
