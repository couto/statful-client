// @flow

import memwatch from "memwatch-next";
import type Statful from "../Statful";

// @see https://github.com/marcominetti/node-memwatch
export default () => (statful: Statful) => {
  memwatch.on("leak", info => {
    statful.counter("memory_leak_warning");
    statful.gauge("memory_leak_growth", info.growth);
  });

  memwatch.on("stats", () => {});
};
