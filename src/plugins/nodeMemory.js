// @flow

import type { Plugin } from "../Types.js.flow";
import type Statful from "../Statful";

// @see https://nodejs.org/dist/latest-v6.x/docs/api/process.html#process_process_memoryusage

type Options = {
  interval: number
};

// We need to override the defaul declaration for setInterval since node's
// implementation is a bit different
type Timeout = {
  ref(): Timeout,
  unref(): Timeout
};

declare function setInterval(
  callback: any,
  ms?: number,
  ...args: any[]
): Timeout;

export default (options: Options = { interval: 10000 }): Plugin => (
  statful: Statful
): Statful => {
  setInterval(() => {
    const memory = process.memoryUsage();

    statful.gauge("memory_rss", memory.rss);
    statful.gauge("memory_heapTotal", memory.heapTotal);
    statful.gauge("memory_heapUsed", memory.heapUsed);
  }, options.interval).unref();

  return statful;
};
