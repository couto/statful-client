// @flow

import blocked from "blocked";
import type { Plugin } from "../Types.js.flow";
import type Statful from "../Statful";

type Options = {
  threshold: number
};

// We need to override the defaul declaration for setInterval since node's
// implementation is a bit different
type Timeout = {
  ref(): Timeout,
  unref(): Timeout
};

declare function setInterval(
  callback: any,
  ms?: number,
  ...args: any[]
): Timeout;

export default (options: Options = { threshold: 10 }): Plugin => (
  statful: Statful
): Statful => {
  blocked(ms => statful.timer("event_loop", ms), options);
  return statful;
};
