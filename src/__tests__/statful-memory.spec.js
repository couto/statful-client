// @flow

import test from "ava";
import { iterate } from "leakage";
import Statful from "../Statful";
import mockBuffer from "./utils";

test.serial("Statful Memory - #put should not leak", t => {
  iterate(() => {
    const statful = new Statful({ buffer: mockBuffer() });
    statful.put("leakage", 1);
  }).printSummary("#put", l => t.log(l));
});

test.serial("Statful Memory - #counter should not leak", t => {
  iterate(() => {
    const statful = new Statful({ buffer: mockBuffer() });
    statful.counter("leakage");
  }).printSummary("#counter", l => t.log(l));
});

test.serial("Statful Memory - #timer should not leak", t => {
  iterate(() => {
    const statful = new Statful({ buffer: mockBuffer() });
    statful.timer("leakage", 1);
  }).printSummary("#timer", l => t.log(l));
});

test.serial("Statful Memory - #gauge should not leak", t => {
  iterate(() => {
    const statful = new Statful({ buffer: mockBuffer() });
    statful.gauge("leakage", 1);
  }).printSummary("#gauge", l => t.log(l));
});
