// @flow
import type { BufferInterface } from "../Types.js.flow";

const mockBuffer = (): BufferInterface<*> => ({
  items: [],
  clear() {
    return this;
  },
  push() {
    return this;
  },
  toArray() {
    return this.items;
  }
});

export default mockBuffer;
