// @flow

import test from "ava";
import Statful from "../Statful";
import mockBuffer from "./utils";

test("Should return a statful instance", t => {
  const statful = new Statful({ buffer: mockBuffer() });
  statful.counter("stuff", 1, { sampleRate: 0.5 });
  t.true(statful instanceof Statful);
});
