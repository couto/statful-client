// @flow

type SubscriberFn = () => mixed;

class Timer {
  timer: ?number;
  subscribers: SubscriberFn[];
  interval: number;

  constructor(interval: number) {
    this.timer = null;
    this.subscribers = [];
    this.interval = interval;
  }

  start(): Timer {
    if (!this.timer) {
      this.timer = setTimeout(() => {
        this.timer = null;
        return Promise.all(this.subscribers.map(subscriber => subscriber()))
          .then(() => this.start())
          .catch(() => this.start());
      }, this.interval);
    }

    return this;
  }

  stop(): Timer {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
    return this;
  }

  onTick(fn: SubscriberFn): Timer {
    this.subscribers.push(fn);
    return this;
  }
}

export default Timer;
