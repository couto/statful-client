// @flow

import dgram from "dgram";
import type { Transport, Metric } from "../Types.js.flow";

type Options = {
  host: string,
  port: number
};

const defaultOptions = {
  host: "127.0.0.1",
  port: 2013
};

export default (options: Options = defaultOptions): Transport => {
  const socket = dgram.createSocket("udp4");

  return (metrics: Metric[]) =>
    new Promise((resolve, reject) => {
      const buffer = Buffer.from(metrics.join("\n"), "utf8");
      const { length } = buffer;
      const { host, port } = options;
      const callback = err => (err ? reject(err) : resolve());

      socket.send(buffer, 0, length, port, host, callback);
    });
};
