// @flow

export default (val: number, max: number = Math.random()): boolean =>
  val < max * 100;
