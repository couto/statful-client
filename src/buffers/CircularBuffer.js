// @flow

import type { BufferInterface } from "../Types.js.flow";

type Pointer = number;

const increment = (pointer: Pointer, length: number): Pointer =>
  (pointer + 1) % length;

class CircularBuffer<I> implements BufferInterface<I> {
  constructor(length: number = 0) {
    this.buffer = new Array(length);
    this.head = 0;
    this.tail = 0;
    this.length = length;
  }

  buffer: Array<I>;
  length: number;
  head: Pointer;
  tail: Pointer;

  push(item: I): CircularBuffer<I> {
    this.buffer[this.head] = item;
    this.head = increment(this.head, this.length);

    return this;
  }

  clear(): CircularBuffer<I> {
    this.head = 0;
    this.tail = 0;

    return this;
  }

  toArray(): Array<I> {
    const list = [];
    let { tail } = this;

    while (tail <= this.head) {
      list.push(this.buffer[tail]);
      tail = increment(tail, this.length);
    }

    return list.filter(i => i);
  }
}

export default CircularBuffer;
