// @flow
import test from "ava";
import CircularBuffer from "../CircularBuffer";

test("Should return an instance of length 5", t => {
  const buffer: CircularBuffer<number> = new CircularBuffer(5);

  t.true(buffer instanceof CircularBuffer);
  t.is(buffer.length, 5);
});

test("#toArray - Should return an array with buffer's contents", t => {
  const buffer: CircularBuffer<number> = new CircularBuffer(5);

  t.deepEqual(buffer.toArray(), []);
});

test("#push - Should add an item to buffer", t => {
  const buffer: CircularBuffer<string> = new CircularBuffer(5);

  buffer.push("foo");

  t.deepEqual(buffer.toArray(), ["foo"]);
});

test("#push - Should override first item in buffer", t => {
  const buffer: CircularBuffer<number> = new CircularBuffer(3);

  buffer.push(0);
  buffer.push(1);
  buffer.push(2);
  buffer.push(3);

  const list = buffer.toArray();
  t.is(list[0], 3);
});
