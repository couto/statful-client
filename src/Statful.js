// @flow

import type {
  BufferInterface,
  Transport,
  Plugin,
  Metric,
  MetricType,
  Tag,
  Aggregation
} from "./Types.js.flow";
import CircularBuffer from "./buffers/CircularBuffer";
import Timer from "./Timer";
import isLess from "./utils";

type MetricDefault = {
  [key: MetricType]: {
    tags: Tag,
    aggregations: Aggregation[]
  }
};

type MetricOptions = {
  sampleRate?: number,
  tags?: Tag
};

type Options = {
  namespace: string,
  sampleRate: number,
  flushSize: number,
  flushInterval: number
};

type StatfulOptions = {
  buffer?: BufferInterface<Metric>,
  transports?: Transport[],
  plugins?: Plugin[],
  namespace?: string,
  sampleRate?: number,
  flushInterval?: number,
  flushSize?: number
};

const defaultOptions = {
  sampleRate: 1,
  namespace: "default",
  flushInterval: 1000,
  flushSize: 100
};

class Statful {
  buffer: BufferInterface<Metric>;
  transports: Transport[];
  plugins: Plugin[];
  options: Options;
  ticker: Timer;

  static MetricType = {
    RAW: "raw",
    COUNTER: "counter",
    TIMER: "timer",
    GAUGE: "gauge"
  };

  static defaults: MetricDefault = {
    raw: { aggregations: [], tags: {} },
    counter: { aggregations: ["sum", "count"], tags: { unit: "ms" } },
    timer: { aggregations: ["avg", "p90", "count"], tags: {} },
    gauge: { aggregations: ["last"], tags: {} }
  };

  constructor(options: StatfulOptions) {
    this.options = Object.assign({}, defaultOptions, options);
    this.buffer =
      this.options.buffer || new CircularBuffer(this.options.flushSize);
    this.transports = this.options.transports || [];
    this.plugins = this.options.plugins || [];
    this.ticker = new Timer(this.options.flushInterval);

    this.plugins.forEach(plugin => plugin(this));
    this.ticker.onTick(() => {
      if (this.buffer.length) {
        this.flush(this.buffer.toArray());
      }
    });
    this.ticker.start();
  }

  _isLess(
    sampleRate: number = this.options.sampleRate,
    odd: number = Math.random()
  ) {
    return odd <= sampleRate;
  }

  flush(metrics: Array<Metric>) {
    const requests = this.transports.map(transport => transport(metrics));

    return Promise.all(requests)
      .then(() => this.buffer.clear())
      .catch(() => {}); // Force silence for the moment
  }

  put(name: string, value: number, options: MetricOptions = {}) {
    const sampleRate = options.sampleRate || this.options.sampleRate;

    if (isLess(sampleRate)) {
      this.buffer.push(
        this.createMetric(Statful.MetricType.RAW, name, value, options)
      );
    }

    return this;
  }

  counter(name: string, value: number = 1, options: MetricOptions = {}) {
    const sampleRate = options.sampleRate || this.options.sampleRate;

    if (isLess(sampleRate)) {
      this.buffer.push(
        this.createMetric(Statful.MetricType.COUNTER, name, value, options)
      );
    }

    return this;
  }

  timer(name: string, value: number, options: MetricOptions = {}) {
    const sampleRate = options.sampleRate || this.options.sampleRate;

    if (isLess(sampleRate)) {
      this.buffer.push(
        this.createMetric(Statful.MetricType.TIMER, name, value, options)
      );
    }

    return this;
  }

  gauge(name: string, value: number, options: MetricOptions = {}) {
    const sampleRate = options.sampleRate || this.options.sampleRate;

    if (isLess(sampleRate)) {
      this.buffer.push(
        this.createMetric(Statful.MetricType.GAUGE, name, value, options)
      );
    }

    return this;
  }

  createMetric(
    type: MetricType,
    name: string,
    value: number,
    options: MetricOptions = {}
  ): Metric {
    const defaults = Statful.defaults[type];
    const metric = {
      timestamp: +new Date(),
      namespace: this.options.namespace,
      sampleRate: this.options.sampleRate,
      aggregationFrequency: [120],
      name: `${type}.${name}`,
      type,
      value
    };

    return Object.assign({}, metric, defaults, options);
  }
}

export default Statful;
