import babel from "rollup-plugin-babel";
import resolve from "rollup-plugin-node-resolve";
import uglify from "rollup-plugin-uglify";

import pkg from "./package.json";

const input = "src/Statful.js";
const isProd = process.env.NODE_ENV === "production";
const minifiedFilename = file => file.replace(".js", ".min.js");

export default [
  {
    input,
    output: {
      file: isProd ? minifiedFilename(pkg.browser) : pkg.browser,
      format: "umd"
    },
    name: "Statful",
    plugins: [
      resolve({ jsnext: true }), // so Rollup can find `ms`
      babel({ exclude: "node_modules/**" }),
      isProd && uglify()
    ].filter(Boolean)
  }
];
